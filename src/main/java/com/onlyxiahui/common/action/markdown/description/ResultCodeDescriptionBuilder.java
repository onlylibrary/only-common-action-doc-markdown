package com.onlyxiahui.common.action.markdown.description;

import java.util.List;

import com.onlyxiahui.common.action.description.bean.ResultCodeData;

/**
 * 
 * <br>
 *
 * @date 2022-04-15 10:16:00<br>
 * @author XiaHui [onxiahui@qq.com]<br>
 * @since 1.0.0
 */
public class ResultCodeDescriptionBuilder {

	public String build(List<ResultCodeData> resultCodes) {
		StringBuilder sb = new StringBuilder();
		if (null != resultCodes && !resultCodes.isEmpty()) {
			sb.append("\n");
			sb.append("**Response-codes:**");
			sb.append("\n");
			sb.append("\n");
			sb.append("\n");
			sb.append("Code|Type|Description");
			sb.append("\n");
			sb.append("---|---|---");
			sb.append("\n");

			for (ResultCodeData rc : resultCodes) {
				String code = rc.getCode();
				String text = rc.getText();
				String type = rc.getType();
				sb.append(null != code ? code : "");
				sb.append("|");
				sb.append(null != type ? type : "");
				sb.append("|");
				sb.append(null != text ? text : "");
				sb.append("\n");
			}
		}
		sb.append("\n");
		return sb.toString();
	}
}
